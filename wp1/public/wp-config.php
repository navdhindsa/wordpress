<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp1');

/** MySQL database username */
define('DB_USER', 'web_user');

/** MySQL database password */
define('DB_PASSWORD', 'mypass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2o0wDH]~sf><X->f%<{]JH{(~ifCNi/c)HTx9211D1I>iARI1oi:aGJmq0fdMjb[');
define('SECURE_AUTH_KEY',  'EOw3Tq,j6X*g H?bvUY1<3SEe(g55OXMQ?-LRDYpM!og/s*t|4U=A!QpT8E-V7rT');
define('LOGGED_IN_KEY',    'Zd>e=_ZehV5bw+vQ4^ZDV;N2(|U!%#E^}&]VG;Zu-6u35<;}C g8[jB+C/RqY5XC');
define('NONCE_KEY',        'k(@?f%d8ng_0Ux}%DC&$ZF^-Tj=N_3Y$Xg}nLuQM71]__(K[Ig?FZs=^3^L$JfpV');
define('AUTH_SALT',        'TS4;^-zscA@WoW.+w(lMsQSla:@9v+MfBj]p!W7*=*Kt[M7#<Q%? H#GY>wjP!w-');
define('SECURE_AUTH_SALT', 'E^OZ@hlA|H!z)V&11r`d*/!M.mx39n4(CN^U>:n}yJk Y0?WE|u1EqBSRn#9T.5%');
define('LOGGED_IN_SALT',   'Q_6aMLx `{^-NfqNFZcx([/w/7f3KfFzv_nVsWC{u&VF{~w[p5hSGke#C?ZHPIxh');
define('NONCE_SALT',       '~^y_-VL.&:{.t9X[^)~K6dVvG18t/5bHd^^4RNBah7TtZAj(.uf(g}4-UL]pLQqK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
