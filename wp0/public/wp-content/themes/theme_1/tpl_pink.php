<?php
//Template Name:pink
 get_header(); ?>
		
		<div class="main">
			<h1>Front page</h1>
			<img src="<?php echo get_template_directory_uri()?>/images/1.jpg" alt="something">
			<?php while(have_posts()) : the_post(); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title('<h1>','</h1>'); ?></a>
				<p><small>Posted by : <?php the_author(); ?> On: <?php the_date(); ?> At: <?php the_time(); ?></small></p>
				<?php the_excerpt('<div>','</div>'); ?>
				<a href="<?php the_permalink(); ?>">Read More</a>

				
			<?php endwhile; ?>
		</div><!-- Main-->
		<?php get_sidebar(); ?>
	</div><!-- Container-->
	<?php get_footer(); ?>