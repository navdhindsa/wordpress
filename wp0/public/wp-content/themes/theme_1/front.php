<?php get_header(); ?>
		
		<div class="main">
			<?php while(have_posts()) : the_post(); ?>
				<?php the_title('<h1>','</h1>'); ?>
				
				<?php the_content('<div>','</div>'); ?>
				
				
			<?php endwhile; ?>
		</div><!-- Main-->
		<?php get_sidebar(); ?>
	</div><!-- Container-->
	<?php get_footer(); ?>