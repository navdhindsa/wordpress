<?php get_header(); ?>
		
		<div class="main">
			<h1>Index</h1>
			<?php while(have_posts()) : the_post(); ?>
				<a href="<?php the_permalink(); ?>"><?php the_title('<h1>','</h1>'); ?></a>
				<p><small>Posted by : <?php the_author(); ?> On: <?php the_date(); ?> At: <?php the_time(); ?></small></p>
				<?php the_excerpt('<div>','</div>'); ?>
				<a href="<?php the_permalink(); ?>">Read More</a>

				
			<?php endwhile; ?>
		</div><!-- Main-->
		<?php get_sidebar(); ?>
	</div><!-- Container-->
	<?php get_footer(); ?>