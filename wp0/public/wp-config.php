<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp0');

/** MySQL database username */
define('DB_USER', 'web_user');

/** MySQL database password */
define('DB_PASSWORD', 'mypass');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#;<]qvC8KBjb,.o!3B<g$aJi&^6]5eZ1NIa!qnf)$L(Jx1~tn~w*>N3vDP=/3aWc');
define('SECURE_AUTH_KEY',  '?+F*W!yCIs2p{GZ#[rp4ySY^TU9<#6cTET4+F1S3QFfTQ0w^U2kf?8ef5gQk{<[o');
define('LOGGED_IN_KEY',    'gT-VgHK?e5&t9i-z#g{1hz7,=bLv[B0w3@O>Sjsh<1$Gr~v&{>LvDs9slC1YH955');
define('NONCE_KEY',        'z=}TnNS.R^%JUw#%o<fO!f+57`{jkB][q=#=IHWE$Ksw/*@z/+.X1ty=c2n&3[H0');
define('AUTH_SALT',        'xyWsK[EKCu7:#F;vPv7*n0(Zq^;kPPTR`%8wy&iqX?#IfCDs&5_HsOs]Ek*!eu6y');
define('SECURE_AUTH_SALT', '_/|+St&)iGlHqVb%3T(_1ch@cAy*`<x~BC,{v&?gV>&JmqkKC #2 )2pG|IQ,$tj');
define('LOGGED_IN_SALT',   '&3a{{5Ew(h6TJFGQ,G6g[Mm1@P,XG24gNe6/p%3|kVb5AkrO/ZF?*2GzpI{#Jw~2');
define('NONCE_SALT',       'V.=JNxYy[;:fg^Ic Q~aqMp%^9m2u{y56!n&-)TAen`4G.nN[Oo;<g}-fiwt;~GE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'w0_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
