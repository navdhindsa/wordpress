<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title><?=get_bloginfo('name')?></title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	
	<link type="text/css" rel="stylesheet" href="<?=get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
</head>
<body>
	<div class="container">

		<header class="main">

			<span class="site_title"><?=get_bloginfo('name')?></span>

		</header>

		<div id="content">

			<div id="primary">
				<?php if(is_archive()) : ?>
					<h1 class="archive_title"><?=get_the_archive_title();?></h1>
				<?php endif; ?>

				<?php if(is_home()) : ?>
					<h1>Our Blog</h1>
				<?php endif; ?>
				<?php while(have_posts()) : the_post(); ?>
				
					<article>
						<?php if(!is_single() && !is_page()) : ?>
						<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
						<?php else :?>
						<?php the_title('<h1>','</h1>'); ?>
						<?php endif; ?>
						<?php the_content(); ?>

					</article><!-- /article -->
				<?php endwhile; ?>
			</div><!-- /primary -->

			<div id="secondary">

				<h3>Menu</h3>

				<?php wp_nav_menu(['container'=>'ul','menu'=>'menu']);?>

				<h3>Archive</h3>
				<ul class="menu"><?php wp_get_archives(); ?></ul>
				

			</div><!-- /secondary -->

		</div><!-- /content -->

		<footer class="main">

				<p class="copyright">Copyright &copy; 2015 by Blogit</p>
		</footer>
		
	</div><!-- /container -->
	<?php wp_footer(); ?>
	</body>
</html>